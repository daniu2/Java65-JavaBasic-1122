package com.ekgc;


import java.util.Scanner;

public class CalSH {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);//System.in标准输入流 指的是键盘
        //System.out 标准输出流 代表屏幕
        System.out.print("请输入你本月的税前薪水:");
        double salary = input.nextDouble();
        double extra = salary - 5000;
        double rate = 0;
        int deduct = 0;
        if(extra <= 3000){
            rate = 0.03;
        }
        else if(extra > 3000 && extra <= 12000){
            rate = 0.1;
            deduct = 210;
        }
        else if(extra > 12000 && extra <= 25000){
            rate = 0.2;
            deduct = 1410;
        }
        else if(extra > 25000 && extra <= 35000){
            rate = 0.25;
            deduct = 2660;
        }
        else if(extra > 35000 && extra <= 55000){
            rate = 0.3;
            deduct = 4410;
        }
        else if(extra > 55000 && extra <= 80000){
            rate = 0.35;
            deduct = 7160;
        }
        else if(extra > 80000){
            rate = 0.45;
            deduct = 15160;
        }
        double actualSalary = salary - ((extra * rate) - deduct);
        System.out.println("您本月实发薪水为：" + actualSalary);

        //JDK 13  switch表达式
       /* String x = "1";
        int result = switch (x) {
            case "1": yield 1;
            case "2": yield 2;
            default: {
                int len = args[1].length();
                yield len;
            }
        };*/
    }




}
